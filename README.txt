
-- SUMMARY --

Commerce Shipping Line Item report. This module provides a VERY simple report.

This module adds a page at Store > Reports > Shipping which displays a summary
and list of all Shipping type line items for orders with a status of Completed
or Pending.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/willisiw/1858352

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1858352


-- REQUIREMENTS --

Dependencies:

* http://drupal.org/project/commerce - Drupal Commerce
* http://drupal.org/project/commerce_shipping - Commerce Shipping

-- INSTALLATION --

* Install module using the zip file.  For further info visit 
  http://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

* After enabling the module, navigate to
  /?q=admin/commerce/reports/shipping

* If you do not see all of your shipped orders, see TROUBLESHOOTING.


-- TROUBLESHOOTING --

* If you do not use Pending or Completed order types for shipped
orders, you may need to modify the query in the module file.
On or near line 32 of the module file you will find a condition
statement with an option array('pending', 'completed').  Modify
the contents of the array to match the way you identify orders
that are shipped.

-- FAQ --


-- CONTACT --

Current maintainers:
* Ian Willis (willisiw) - http://github.com/willisiw
