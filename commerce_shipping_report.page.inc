<?php
/**
 * @file
 * Module for displaying a ridiculously simple shipping report.
 */

// commerce_shipping_report_page.
/**
 * Displays a simple shipping report.
 *
 * @return string
 *   String of HTML.
 */
function commerce_shipping_report_page() {
  // Tracking my orders.
  global $user;
  $build = array();
  $summaryarray = array();

  $query = db_select('commerce_order', 'c');
  $query
    ->condition('c.status', array('pending', 'completed'), 'IN')
    ->fields('c');
  $query
    ->orderBy('c.order_id', 'DESC');
  $result = $query->execute();

  foreach ($result as $row) {
    $offer = '';
    $shipping_price_array = array();
    // Add a nice distinct color block to each order.
    $color = drupal_substr(md5($row->order_id), 0, 6);
    $this_order = commerce_order_load($row->order_id);
    $this_user = user_load($row->uid);
    $this_user_profile = commerce_customer_profile_load($this_order->commerce_customer_shipping['und']['0']['profile_id']);
    $this_order_date = date("Y-m", $row->created);

    // Accumulate line item ids from the order.
    $line_item_array = array();
    foreach ($this_order->commerce_line_items['und'] as $k => $v) {
      $line_item_array[] = $v['line_item_id'];
    }
    // Load line items for this order.
    $line_items = commerce_line_item_load_multiple($line_item_array, $conditions = array(), $reset = FALSE);

    // Loop through the line items and select only the first line item
    // for shipping.
    foreach ($line_items as $k => $v) {
      if ($v->type == 'shipping') {
        foreach ($v->commerce_total['und'] as $k1 => $v1) {
          // Collect shipping per order.
          $shipping_price_array[] = $v1['amount'];
          // Accumulate shipping per month.
          if (isset($summaryarray[$this_order_date])) {
            $summaryarray[$this_order_date] += $v1['amount'];
          }
          else {
            $summaryarray[$this_order_date] = $v1['amount'];
          }
        }
      }
    }
    $color = drupal_substr(md5($this_order_date), 0, 6);
    $rows[] = array(
      '<div style="width:20px;height:20px;float:left;margin-right:10px;background:#' . $color . '"></div>' .
      l($this_order->order_id, 'admin/commerce/orders/' . $this_order->order_id),
      l($this_order->mail, 'user/' . $this_order->uid),
      $this_user_profile->commerce_customer_address['und']['0']['locality'] . ', ' .
      $this_user_profile->commerce_customer_address['und']['0']['administrative_area'] . ' ' .
      $this_user_profile->commerce_customer_address['und']['0']['postal_code'],
      format_date($row->created),
      format_date($row->changed),
      '<div style="width:20px;height:20px;float:right;margin-right:10px;background:#' . $color . '"></div><div style="text-align:right;float:right;margin-right:10px">' . money_format('%i', array_sum($shipping_price_array) * .01) . '</div>',
    );

  }

  if (empty($rows)) {
    $rows[] = array(
      '<div style="width:20px;height:20px;float:left;margin-right:10px;background:#666666"></div>',
      'No orders have shipped, or you have no active orders.',
      '',
      '',
      '',
      '',
    );
    $summaryrows[] = array(
      '<div style="width:20px;height:20px;float:left;margin-right:10px;background:#666666"></div>',
      'No orders have shipped, or you have no active orders.',
      '',
    );
  }
  else {
    // Populate the summary rows by month.
    foreach ($summaryarray as $k => $v) {
      $color = drupal_substr(md5($k), 0, 6);
      $summaryrows[] = array(
        "<div style=\"width:20px;height:20px;float:left;margin-right:10px;background:#$color\"></div>$k",
        '<div style="float:right;margin-right:10px;">' . money_format('%i', $v * .01) . '</div>',
      );
    }
  }

  $summaryheader = array('Month', 'Shipping Total');
  $header = array(
    'Order ID',
    'Customer',
    'Ship To',
    'Created',
    'Changed',
    'Shipping');

  // Theme the output.
  $output = "<h4>This is a very simple shipping report.  Shipping is compiled from orders that are Completed or Pending.</h4></br>";
  $output .= theme('table', array('header' => $summaryheader, 'rows' => $summaryrows));
  $output .= theme('table', array('header' => $header, 'rows' => $rows)) . theme('pager');

  return $output;
}
